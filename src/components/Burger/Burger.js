import React from 'react';
import OneIngredient from '../OneIngredient/OneIngredient'
import './Burger.css';


const Burger = props => {
	return (
		<div className="Burger-container">
			<div className="Burger-image">
				<div className="Burger">
					<div className="BreadTop">
						<div className="Seeds1"/>
						<div className="Seeds2"/>
					</div>
					{
						props.ing.map( (ing,index) => {
							return (
								<OneIngredient
									key={index}
								    name={ing.name}
								    count={ing.count}
								/>
							)
						})
					}
					<div className="BreadBottom"/>
				</div>
			</div>
			<p className="Total-cost">Итого: <span>{props.price}</span> сом</p>
		</div>
	)
};

export default Burger;
