import React from 'react';
import './Ingredients.css';


const Ingredients = props => {

	return (
		<li className="Ingredients">
			<span className={'Ingredients-icon ' + props.name} />
			<span>
				<button onClick={props.click} className="Ingredients-name">{props.ru}</button>
			</span>
			<span className="Ingredients-count">&times; {props.count}</span>
			<span className="Ingredients-cost">{props.cost} сом</span>
			<span>
				<button onClick={props.remove} className="Ingredients-remove" />
			</span>
		</li>
	)
};

export default Ingredients;
