import React from 'react';

const OneIngredient = props => {

	const getIngredient = () => {

		let result = [];
		for (let i = 0; i < props.count; ++i) {
			result.push(props.name)
		}

		return result;
	};
	return (
		getIngredient().map((name, index) => {
			return (
				<div key={index} className={`${name}`} />
			)
		})
	)
};

export default OneIngredient;