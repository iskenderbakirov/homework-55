import React, {Component} from 'react';
import Ingredients from './components/Ingredients/Ingredients';
import Burger from './components/Burger/Burger';

class App extends Component {
	state = {
		INGREDIENTS: [
			{id: 'ing1', ru: 'Мясо',  name: 'meat',   count: 0, cost: 50},
			{id: 'ing2', ru: 'Салат', name: 'salad',  count: 0, cost: 5},
			{id: 'ing3', ru: 'Сыр',   name: 'cheese', count: 0, cost: 20},
			{id: 'ing4', ru: 'Бекон', name: 'bacon',  count: 0, cost: 30}
		],
		totalPrice: 20
	};

	calculate = (name) => {
		let totalPrice = this.state.totalPrice;
		console.log(name);

		for (let i = 0; i < this.state.INGREDIENTS.length; ++i) {
			if(this.state.INGREDIENTS[i].name === name) {
				totalPrice += this.state.INGREDIENTS[i].cost;
			}
		}

		return totalPrice;
	};


	addIng = (event, item) => {
		const ingCopy = [...this.state.INGREDIENTS];
		const itemCopy = {...this.state.INGREDIENTS[item]};

		itemCopy.count ++;

		ingCopy[item] = itemCopy;

		let price = this.calculate(ingCopy[item].name);

		this.setState({ INGREDIENTS: ingCopy, totalPrice: price });
	};

	removeIng = (event, item) => {
		const ingCopy = [...this.state.INGREDIENTS];
		const itemCopy = {...this.state.INGREDIENTS[item]};

		if (itemCopy.count !== 0) {
			itemCopy.count --;
		} else {
			itemCopy.count = 0;
		}


		ingCopy[item] = itemCopy;

		let price = this.calculate(itemCopy);

		this.setState({ INGREDIENTS: ingCopy, totalPrice: price });
	};

	render() {

		return (
			<div className="App">
				<h1>Делайте ваш заказ!</h1>
				<div className="container">
					<div className="col-6">
						<h2>Ингредиенты</h2>
						<div className="content">
							<ul>
								{
									this.state.INGREDIENTS.map( (item, id) => {
										return (
											<Ingredients
												key={item.id}
												name={item.name}
												ru={item.ru}
												count={item.count}
												cost={item.cost}
												click={event => this.addIng(event, id)}
												remove={event => this.removeIng(event, id)}
											/>
										)
									})
								}
							</ul>
						</div>
					</div>
					<div className="col-6">
						<h2>Ваш Бургер</h2>
						<div className="content">
							<Burger
								price={this.state.totalPrice}
							    ing={this.state.INGREDIENTS}
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default App;
